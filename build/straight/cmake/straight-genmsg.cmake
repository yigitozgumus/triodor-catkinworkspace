# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "straight: 0 messages, 1 services")

set(MSG_I_FLAGS "-Istd_msgs:/opt/ros/jade/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(straight_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/yigit/catkin_ws/src/straight/srv/Status.srv" NAME_WE)
add_custom_target(_straight_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "straight" "/home/yigit/catkin_ws/src/straight/srv/Status.srv" ""
)

#
#  langs = gencpp;geneus;genlisp;genpy
#

### Section generating for lang: gencpp
### Generating Messages

### Generating Services
_generate_srv_cpp(straight
  "/home/yigit/catkin_ws/src/straight/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/straight
)

### Generating Module File
_generate_module_cpp(straight
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/straight
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(straight_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(straight_generate_messages straight_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/straight/srv/Status.srv" NAME_WE)
add_dependencies(straight_generate_messages_cpp _straight_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(straight_gencpp)
add_dependencies(straight_gencpp straight_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS straight_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages

### Generating Services
_generate_srv_eus(straight
  "/home/yigit/catkin_ws/src/straight/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/straight
)

### Generating Module File
_generate_module_eus(straight
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/straight
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(straight_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(straight_generate_messages straight_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/straight/srv/Status.srv" NAME_WE)
add_dependencies(straight_generate_messages_eus _straight_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(straight_geneus)
add_dependencies(straight_geneus straight_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS straight_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages

### Generating Services
_generate_srv_lisp(straight
  "/home/yigit/catkin_ws/src/straight/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/straight
)

### Generating Module File
_generate_module_lisp(straight
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/straight
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(straight_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(straight_generate_messages straight_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/straight/srv/Status.srv" NAME_WE)
add_dependencies(straight_generate_messages_lisp _straight_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(straight_genlisp)
add_dependencies(straight_genlisp straight_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS straight_generate_messages_lisp)

### Section generating for lang: genpy
### Generating Messages

### Generating Services
_generate_srv_py(straight
  "/home/yigit/catkin_ws/src/straight/srv/Status.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/straight
)

### Generating Module File
_generate_module_py(straight
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/straight
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(straight_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(straight_generate_messages straight_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/yigit/catkin_ws/src/straight/srv/Status.srv" NAME_WE)
add_dependencies(straight_generate_messages_py _straight_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(straight_genpy)
add_dependencies(straight_genpy straight_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS straight_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/straight)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/straight
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
add_dependencies(straight_generate_messages_cpp std_msgs_generate_messages_cpp)

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/straight)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/straight
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
add_dependencies(straight_generate_messages_eus std_msgs_generate_messages_eus)

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/straight)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/straight
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
add_dependencies(straight_generate_messages_lisp std_msgs_generate_messages_lisp)

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/straight)
  install(CODE "execute_process(COMMAND \"/usr/bin/python\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/straight\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/straight
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
add_dependencies(straight_generate_messages_py std_msgs_generate_messages_py)
