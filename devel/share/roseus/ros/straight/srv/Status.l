;; Auto-generated. Do not edit!


(when (boundp 'straight::Status)
  (if (not (find-package "STRAIGHT"))
    (make-package "STRAIGHT"))
  (shadow 'Status (find-package "STRAIGHT")))
(unless (find-package "STRAIGHT::STATUS")
  (make-package "STRAIGHT::STATUS"))
(unless (find-package "STRAIGHT::STATUSREQUEST")
  (make-package "STRAIGHT::STATUSREQUEST"))
(unless (find-package "STRAIGHT::STATUSRESPONSE")
  (make-package "STRAIGHT::STATUSRESPONSE"))

(in-package "ROS")





(defclass straight::StatusRequest
  :super ros::object
  :slots ())

(defmethod straight::StatusRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass straight::StatusResponse
  :super ros::object
  :slots (_status ))

(defmethod straight::StatusResponse
  (:init
   (&key
    ((:status __status) nil)
    )
   (send-super :init)
   (setq _status __status)
   self)
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:serialization-length
   ()
   (+
    ;; bool _status
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _status
       (if _status (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _status
     (setq _status (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass straight::Status
  :super ros::object
  :slots ())

(setf (get straight::Status :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get straight::Status :datatype-) "straight/Status")
(setf (get straight::Status :request) straight::StatusRequest)
(setf (get straight::Status :response) straight::StatusResponse)

(defmethod straight::StatusRequest
  (:response () (instance straight::StatusResponse :init)))

(setf (get straight::StatusRequest :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get straight::StatusRequest :datatype-) "straight/StatusRequest")
(setf (get straight::StatusRequest :definition-)
      "
---
bool status

")

(setf (get straight::StatusResponse :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get straight::StatusResponse :datatype-) "straight/StatusResponse")
(setf (get straight::StatusResponse :definition-)
      "
---
bool status

")



(provide :straight/Status "3a1255d4d998bd4d6585c64639b5ee9a")


