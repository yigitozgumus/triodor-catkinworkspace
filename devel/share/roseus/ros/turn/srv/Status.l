;; Auto-generated. Do not edit!


(when (boundp 'turn::Status)
  (if (not (find-package "TURN"))
    (make-package "TURN"))
  (shadow 'Status (find-package "TURN")))
(unless (find-package "TURN::STATUS")
  (make-package "TURN::STATUS"))
(unless (find-package "TURN::STATUSREQUEST")
  (make-package "TURN::STATUSREQUEST"))
(unless (find-package "TURN::STATUSRESPONSE")
  (make-package "TURN::STATUSRESPONSE"))

(in-package "ROS")





(defclass turn::StatusRequest
  :super ros::object
  :slots ())

(defmethod turn::StatusRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass turn::StatusResponse
  :super ros::object
  :slots (_status ))

(defmethod turn::StatusResponse
  (:init
   (&key
    ((:status __status) nil)
    )
   (send-super :init)
   (setq _status __status)
   self)
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:serialization-length
   ()
   (+
    ;; bool _status
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _status
       (if _status (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _status
     (setq _status (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass turn::Status
  :super ros::object
  :slots ())

(setf (get turn::Status :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get turn::Status :datatype-) "turn/Status")
(setf (get turn::Status :request) turn::StatusRequest)
(setf (get turn::Status :response) turn::StatusResponse)

(defmethod turn::StatusRequest
  (:response () (instance turn::StatusResponse :init)))

(setf (get turn::StatusRequest :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get turn::StatusRequest :datatype-) "turn/StatusRequest")
(setf (get turn::StatusRequest :definition-)
      "
---
bool status

")

(setf (get turn::StatusResponse :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get turn::StatusResponse :datatype-) "turn/StatusResponse")
(setf (get turn::StatusResponse :definition-)
      "
---
bool status

")



(provide :turn/Status "3a1255d4d998bd4d6585c64639b5ee9a")


