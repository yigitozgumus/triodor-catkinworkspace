;; Auto-generated. Do not edit!


(when (boundp 'wall_follower::Status)
  (if (not (find-package "WALL_FOLLOWER"))
    (make-package "WALL_FOLLOWER"))
  (shadow 'Status (find-package "WALL_FOLLOWER")))
(unless (find-package "WALL_FOLLOWER::STATUS")
  (make-package "WALL_FOLLOWER::STATUS"))
(unless (find-package "WALL_FOLLOWER::STATUSREQUEST")
  (make-package "WALL_FOLLOWER::STATUSREQUEST"))
(unless (find-package "WALL_FOLLOWER::STATUSRESPONSE")
  (make-package "WALL_FOLLOWER::STATUSRESPONSE"))

(in-package "ROS")





(defclass wall_follower::StatusRequest
  :super ros::object
  :slots ())

(defmethod wall_follower::StatusRequest
  (:init
   (&key
    )
   (send-super :init)
   self)
  (:serialization-length
   ()
   (+
    0
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;;
   self)
  )

(defclass wall_follower::StatusResponse
  :super ros::object
  :slots (_status ))

(defmethod wall_follower::StatusResponse
  (:init
   (&key
    ((:status __status) nil)
    )
   (send-super :init)
   (setq _status __status)
   self)
  (:status
   (&optional __status)
   (if __status (setq _status __status)) _status)
  (:serialization-length
   ()
   (+
    ;; bool _status
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _status
       (if _status (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _status
     (setq _status (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(defclass wall_follower::Status
  :super ros::object
  :slots ())

(setf (get wall_follower::Status :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get wall_follower::Status :datatype-) "wall_follower/Status")
(setf (get wall_follower::Status :request) wall_follower::StatusRequest)
(setf (get wall_follower::Status :response) wall_follower::StatusResponse)

(defmethod wall_follower::StatusRequest
  (:response () (instance wall_follower::StatusResponse :init)))

(setf (get wall_follower::StatusRequest :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get wall_follower::StatusRequest :datatype-) "wall_follower/StatusRequest")
(setf (get wall_follower::StatusRequest :definition-)
      "
---
bool status

")

(setf (get wall_follower::StatusResponse :md5sum-) "3a1255d4d998bd4d6585c64639b5ee9a")
(setf (get wall_follower::StatusResponse :datatype-) "wall_follower/StatusResponse")
(setf (get wall_follower::StatusResponse :definition-)
      "
---
bool status

")



(provide :wall_follower/Status "3a1255d4d998bd4d6585c64639b5ee9a")


