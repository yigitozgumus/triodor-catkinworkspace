cmake_minimum_required(VERSION 2.8.3)
project(door_control)


find_package(catkin REQUIRED COMPONENTS
  roscpp
  std_msgs
  vrep_common
)

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR}/lib)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
  ${catkin_INCLUDE_DIRS}
)

## Declare a cpp library
# add_library(door_control
#   src/${PROJECT_NAME}/door_control.cpp
# )

## Declare a cpp executable
 add_executable(doorControl src/door_control_node.cpp)

## Add cmake target dependencies of the executable/library
## as an example, message headers may need to be generated before nodes
 add_dependencies(doorControl door_control_generate_messages_cpp)

## Specify libraries to link a library or executable target against
 target_link_libraries(doorControl
   ${catkin_LIBRARIES}
 )


#############
## Testing ##
#############

## Add gtest based cpp test target and link libraries
# catkin_add_gtest(${PROJECT_NAME}-test test/test_door_control.cpp)
# if(TARGET ${PROJECT_NAME}-test)
#   target_link_libraries(${PROJECT_NAME}-test ${PROJECT_NAME})
# endif()

## Add folders to be run by python nosetests
# catkin_add_nosetests(test)
