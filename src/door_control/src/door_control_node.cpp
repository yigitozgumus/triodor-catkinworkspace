//All include files
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <ros/ros.h>
#include <iostream>
#include <cstdio>
#include <cmath>
#include <math.h>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Float32.h"
#include "std_msgs/Float64.h"
#include "geometry_msgs/Point.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Bool.h"

#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"
#include "vrep_common/simRosEnableSubscriber.h"


#define PI 3.14159265

float motorValue = 0.0 ;
bool simulationRunning = true;
float simulationTime=0.0f;

void doorCallBack(const std_msgs::Bool::ConstPtr& doorOpen){
	std::cout << "i checked it" << std::endl;
	if(doorOpen->data == 1){
		motorValue = -2.0 ;
	}else{
		motorValue = 2.0 ;
	}
}
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info){
	simulationTime=info->simulationTime.data;
	simulationRunning = (info->simulatorState.data&1)!=0;
}

int main(int argc,char* argv[]){

	//handles
	int motorHandle ;

	if(argc >= 2){
		motorHandle = atoi(argv[1]);
	}
	else {
		std::cout << "Indicate following arguments: MotorHandle "<< std::endl;
		sleep(5000);
		return 0 ;
	}

	//Creation of the Ros Node
	int _argc = 0 ;
	char** _argv = NULL ;
	std::string nodeName("doorControl");

	// ROS node initialization
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	//Create a node handle
	ros::NodeHandle nh("doorControl");
	
	//Subscription to V-REP's info stream
	//SUBSCRIBER FOR V-REP

	ros::ServiceClient client_enableSubscriber=nh.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
	vrep_common::simRosEnableSubscriber srv_enableSubscriber;
	srv_enableSubscriber.request.topicName="/"+nodeName+"/door"; // the topic name
	srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
	srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type
	srv_enableSubscriber.request.auxInt1 = -1;
	srv_enableSubscriber.request.auxInt2 = -1 ;
	srv_enableSubscriber.request.auxString = "";
	//SUBSCRIBER FOR THE COVER TURN (FOR V-REP)

	if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1)){
			

	ros::Subscriber defaultSub=nh.subscribe("/vrep/info",1,infoCallback);
	ros::Subscriber motionTrigger=nh.subscribe("/doorControl/openClose",1,doorCallBack);

	ros::Publisher doorMotorPub = nh.advertise<vrep_common::JointSetStateData>("door",1);

	vrep_common::JointSetStateData doorMessage ;

	while (ros::ok()&&simulationRunning)
		{ // this is the control loop
			doorMessage.handles.data.push_back(motorHandle);
			doorMessage.setModes.data.push_back(2);
			doorMessage.values.data.push_back(motorValue);
			doorMotorPub.publish(doorMessage);

			// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				usleep(1000);
		}
	}
	return 0 ;
}