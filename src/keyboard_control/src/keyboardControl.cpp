// Copyright 2006-2015 Coppelia Robotics GmbH. All rights reserved.
// marc@coppeliarobotics.com
// www.coppeliarobotics.com
//
// -------------------------------------------------------------------
// THIS FILE IS DISTRIBUTED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
// WARRANTY. THE USER WILL USE IT AT HIS/HER OWN RISK. THE ORIGINAL
// AUTHORS AND COPPELIA ROBOTICS GMBH WILL NOT BE LIABLE FOR DATA LOSS,
// DAMAGES, LOSS OF PROFITS OR ANY OTHER KIND OF LOSS WHILE USING OR
// MISUSING THIS SOFTWARE.
//
// You are free to use/modify/distribute this file for whatever purpose!
// -------------------------------------------------------------------
//
// This file was automatically created for V-REP release V3.2.1 on May 3rd 2015

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <termios.h>
#include <ros/ros.h>
#include <iostream>
#include <ctime>
#include <cstdio>
#include <ctime>
#include "../include/v_repConst.h"
#include "geometry_msgs/Twist.h"

// Used data structures:
#include "vrep_common/VrepInfo.h"
#include "vrep_common/JointSetStateData.h"

// Used API services:
#include "vrep_common/simRosEnablePublisher.h"
#include "vrep_common/simRosEnableSubscriber.h"
#include <ctime>



// Global variables (modified by topic subscribers):
bool simulationRunning=true;
float simulationTime=0.0f;
float angularData[3] = {};
float linearData[3] = {};
std::clock_t start;
    double duration;
bool test =false;
ros::Timer timeCheck;


// Topic subscriber callbacks:
void infoCallback(const vrep_common::VrepInfo::ConstPtr& info)
{
	simulationTime=info->simulationTime.data;
	simulationRunning=(info->simulatorState.data&1)!=0;
}
void timerCallBack(const ros::TimerEvent& event){
	
	angularData[2] = 0;
	linearData[0] = 0;
	std::cout << "make it zero\n";
	timeCheck.stop();
	


}
void locationCallback(const geometry_msgs::Twist::ConstPtr& info){
	//start = std::clock();
	if(timeCheck.isValid()){
	timeCheck.stop();
	timeCheck.start();
	}
	angularData[2] = info->angular.z;
	linearData[0] = info->linear.x;
	
}


// Main code:
int main(int argc,char* argv[])
{

	int kfd = 0;
	 struct termios cooked, raw;
	// The joint handles and proximity sensor handles are given in the argument list
	// (when V-REP launches this executable, V-REP will also provide the argument list)
	int leftMotorHandle;
	int rightMotorHandle;

	
	if (argc>=3)
	{
		leftMotorHandle=atoi(argv[1]);
		rightMotorHandle=atoi(argv[2]);
	}
	else
	{
		printf("Indicate following arguments: 'leftMotorHandle rightMotorHandle sensorHandle'!\n");
		sleep(5000);
		return 0;
	}

	// Create a ROS node. The name has a random component:
	int _argc = 0;
	char** _argv = NULL;
	struct timeval tv;
	unsigned int timeVal=0;
	if (gettimeofday(&tv,NULL)==0)
		timeVal=(tv.tv_sec*1000+tv.tv_usec/1000)&0x00ffffff;
	std::string nodeName("keyControl");
	std::string randId(boost::lexical_cast<std::string>(timeVal+int(999999.0f*(rand()/(float)RAND_MAX))));
	nodeName+=randId;
	ros::init(_argc,_argv,nodeName.c_str());

	if(!ros::master::check())
		return(0);

	ros::NodeHandle node("~");
	timeCheck = node.createTimer(ros::Duration(2), timerCallBack,false);
	printf("rosBubbleRob just started with node name %s\n",nodeName.c_str());

	// 1. Let's subscribe to V-REP's info stream (that stream is the only one enabled by default,
	// and the only one that can run while no simulation is running):
	//ros::Subscriber subInfo=node.subscribe("/vrep/info",1,infoCallback);
	ros::Subscriber locInfo = node.subscribe("/turtle1/cmd_vel",1,locationCallback);

	
		// 4. Let's tell V-REP to subscribe to the motor speed topic (publisher to that topic will be created further down):
		ros::ServiceClient client_enableSubscriber=node.serviceClient<vrep_common::simRosEnableSubscriber>("/vrep/simRosEnableSubscriber");
		vrep_common::simRosEnableSubscriber srv_enableSubscriber;

		// srv_enableSubscriber.request.topicName="/turtle1/cmd_vel"; // the topic name
		// srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		// srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_twist_command; // the subscriber type
		srv_enableSubscriber.request.topicName="/"+nodeName+"/wheels"; // the topic name
		srv_enableSubscriber.request.queueSize=1; // the subscriber queue size (on V-REP side)
		srv_enableSubscriber.request.streamCmd=simros_strmcmd_set_joint_state; // the subscriber type
		if ( client_enableSubscriber.call(srv_enableSubscriber)&&(srv_enableSubscriber.response.subscriberID!=-1) )
		{	// ok, the service call was ok, and the subscriber was succesfully started on V-REP side
			// V-REP is now listening to the desired motor joint states
							
			// 5. Let's prepare a publisher of those motor spe
			ros::Publisher motorSpeedPub=node.advertise<vrep_common::JointSetStateData>("wheels",1);
   			
   					
					vrep_common::JointSetStateData motorSpeeds;

				float desiredLeftMotorSpeed = 0;
					float desiredRightMotorSpeed =0;	
				while (ros::ok()&&simulationRunning)
			{ 
				
					if(linearData[0] ==2){
						desiredLeftMotorSpeed = 4;
						desiredRightMotorSpeed = 4;
						
						std::cout << "moving forward\n";
					
						
					}
					else if(linearData[0] == -2){
						desiredLeftMotorSpeed = -4;
						desiredRightMotorSpeed = -4;
						
						std::cout << "moving back\n";
					
																
					}
					else if(angularData[2] == 2){
						desiredLeftMotorSpeed = -4;
						desiredRightMotorSpeed = +4;
						
						std::cout << "turning left\n";
						
						
					}
					else if(angularData[2] == -2){
						desiredLeftMotorSpeed = 4;
						desiredRightMotorSpeed = -4;
						
						std::cout << "turning right\n";
					
					
					}else{
						desiredLeftMotorSpeed = 0;
						desiredRightMotorSpeed = 0;
						
					}
					// duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
					//  if(duration > 1){
					//  	std::cout>> "1 second is elapsed\n";
					// 	desiredLeftMotorSpeed = 0;
					// 	desiredRightMotorSpeed = 0;
					// }

																																							
			   
				motorSpeeds.handles.data.push_back(leftMotorHandle);
				motorSpeeds.handles.data.push_back(rightMotorHandle);
				motorSpeeds.setModes.data.push_back(2); // 2 is the speed mode
				motorSpeeds.setModes.data.push_back(2);
				motorSpeeds.values.data.push_back(desiredLeftMotorSpeed);
				motorSpeeds.values.data.push_back(desiredRightMotorSpeed);
			
				motorSpeedPub.publish(motorSpeeds);	
				
				// handle ROS messages:
				ros::spinOnce();

				// sleep a bit:
				
				usleep(5000);				
					
			}
		}
		
	
    ros::shutdown();
   
	printf("rosBubbleRob just ended!\n");
	return(0);
}

